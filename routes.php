<?php
  function call($controller, $action) {
    $strViewName = false;
    $aData = array(); // Note: One side-effect of doing it this way is that controllers must now support a method for gathering data.

    // require the file that matches the controller name
    require_once('controllers/' . $controller . '_controller.php');

    // create a new instance of the needed controller
    // JAM: Might be nice to decouple this check somehow...
    switch($controller) {
      case 'pages':
        $controller = new PagesController($aData);
        $strViewName = "views/pages/home.php";
        break;
      case 'newCustomer':
        require_once('models/countries.php');
        require_once('models/states.php');
        require_once('models/customer.php'); // JAM: Typo here, case-sensitivity.
        $controller = new NewCustomerController($aData);

        if($action == "customerList" || $action == "deleteCustomer"){
          $strViewName = "views/newcustomer/customerList.php";
        }
        else{
          $strViewName = "views/newcustomer/home.php";
          /* if(isset($_POST["edit"])) {
            $strViewName = "views/newcustomer/customerEditForm.php";
          }
          else{
            $strViewName = "views/newcustomer/home.php";
          } */
        }

        break;
    }

    // call the action
    $controller->{ $action }();

    // echo "<pre>Data:\n" . print_r($aData, true) . "</pre>\n";
    // JAM: Group all of our output into one place.
    require_once("views/layout.php");
  }

  // just a list of the controllers we have and their actions
  // we consider those "allowed" values
  $controllers = array('pages' => ['home', 'error'],'newCustomer'=>['home', 'error','insertCustomer','customerList','updateCustomer']);

  // check that the requested controller and action are both allowed
  // if someone tries to access something else he will be redirected to the error action of the pages controller
  if(isValidController($controller, $action)){
    call($controller, $action);
  }
  else{
    call('pages', 'error');
  }

  /* if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
      call($controller, $action);
    } else {
      call('pages', 'error');
    }
  } else {
    call('pages', 'error');
  } */
?>