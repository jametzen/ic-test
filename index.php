<?php
  ini_set("display_errors", 1);
  error_reporting(E_ALL);

  // JAM: A quick and dirty structure for passing information around the app.
  $aMsgs = array("error" => array(), "warning" => array(), "notice" => array());

  // JAM: Store all of our reusable functionality in one place.
  require_once("functions.php");

  require_once('connection.php');

  // JAM: Declaring here just for use in the routes file seems a little wonky to me. I think the best thing to do might be to make a controller class that could check a given controller
  // name, etc for validation, however I'm not sure I'm going to have time to implement such a thing.
  if (isset($_GET['controller']) && isset($_GET['action'])) {
    // Unsafe?
    $controller = $_GET['controller'];
    $action     = $_GET['action'];
  } else {
    $controller = 'pages';
    $action     = 'home';
  }

  // require_once('views/layout.php');
  // To ensure adherence to the MVC philisophy, we route first, then load views. This keeps the view processing at the "end" of the sequence and completely separates them from the logic.
  require_once("routes.php");
?>