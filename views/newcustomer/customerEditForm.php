<?php echo "<pre>Data in view:\n" . print_r($aData, true) . "</pre>\n"; ?>

<div class="container box body-content" >
<form action="?controller=newCustomer&action=updateCustomer" method="post">
<!-- @todo This form is pretty redundant to the customer insert form, refactor it to use the same markup system. -->
<h2><strong>Edit Customer</strong></h2>
<div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Customer Title
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" value="<?php echo $aData["objCustomer"]->title; // @todo Convert to drop-down ?>" class="form-control" name="CustomerTitle">
                </div>
</div>
<div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Customer Name
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" value ='<?php echo $aData["objCustomer"]->name; // @todo Split into multiple fields. ?>' required class="form-control" name="CustomerName">
                </div>
            </div>
<div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Address
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" value ='<?php echo $aData["objCustomer"]->address ?>' name="Address">
                </div>
</div>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       City
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" value ='<?php echo $aData["objCustomer"]->city ?>' name="City">
                </div>
            </div>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       State
                    </strong>
                </div>
                <div class="col-md-6">
                    <select required value ='<?php echo $aData["objCustomer"]->stateID ?>' name=" state" class="form-control">
<option value="">--Select--</option>
	<?php foreach($aData["aStates"] as $state) { ?>
        <option 

        value='<?php echo $state->id ?>' <?php if($aData["objCustomer"]->stateID == $state->id)  echo ' selected="selected"' ?> ><?php echo $state->name ?></option>
    <?php }?>
</select>
                </div>
            </div>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                      Postal Code
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="PostalCode" value ='<?php echo $aData["objCustomer"]->postalCode ?>'>
                </div>
            </div>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Country
                    </strong>
                </div>
                <div class="col-md-6">
                    <select required name="country" class="form-control" value ='<?php echo $aData["objCustomer"]->countryID ?>'>
<option value="">--Select--</option>
    <?php foreach($aData["aCountries"] as $country) { ?>
        <option value='<?php echo $country->id ?>' <?php if($aData["objCustomer"]->countryID == $country->id)  echo ' selected="selected"' ?>><?php echo $country->name ?>
        </option>
    <?php }?>
</select>
                </div>
            </div>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Email
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" required class="form-control" value ='<?php echo $aData["objCustomer"]->email ?>' name="Email">
                </div>
            </div>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Modified By
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" required name="ModifiedBy">
                </div>
  </div>

<input type="hidden" name="CustomerID" value='<?php echo $aData["objCustomer"]->id ?>'>
<input type="submit" name="update" value="Update" class="btn btn-primary" text="Update"/> 
<a>Cancel</a> 
</form>
 </div>
