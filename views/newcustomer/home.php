<div class="container box body-content" >
<form action="?controller=newCustomer&action=<?php echo ($action == "insertCustomer") ? "insertCustomer" : "updateCustomer"; ?>" method="post">
<h2><strong><?php if($action == "insertCustomer"){ ?>Add<?php }else {?>Edit<?php } ?> Customer</strong></h2>
<div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Customer Title
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="CustomerTitle" value="<?php echo $aData["objCustomer"]->title; ?>">
                </div>
</div>
<div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Customer Name
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" required class="form-control" name="CustomerName" value="<?php echo $aData["objCustomer"]->name; ?>">
                </div>
            </div>
<div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Address
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control"  name="Address" value="<?php echo $aData["objCustomer"]->address; ?>">
                </div>
</div>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       City
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control"  name="City" value="<?php echo $aData["objCustomer"]->city; ?>">
                </div>
            </div>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       State
                    </strong>
                </div>
                <div class="col-md-6">
                    <select required  name=" state" class="form-control">
<option value="">--Select--</option>
	<?php foreach($aData["aStates"] as $state) { ?>
        <option value='<?php echo $state->id ?>' <?php if($state->id == $aData["objCustomer"]->stateID){ echo "selected"; }?>><?php echo $state->name ?></option>
    <?php }?>
</select>
                </div>
            </div>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                      Postal Code
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="PostalCode" value="<?php echo $aData["objCustomer"]->postalCode; ?>">
                </div>
            </div>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Country
                    </strong>
                </div>
                <div class="col-md-6">
                    <select required name="country" class="form-control" >
<option value="">--Select--</option>
    <?php foreach($aData["aCountries"] as $country) { ?>
        <option value='<?php echo $country->id ?>' <?php if($country->id == $aData["objCustomer"]->countryID){ echo "selected"; }?>><?php echo $country->name ?>
        </option>
    <?php }?>
</select>
                </div>
            </div>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Email
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" required class="form-control"  name="Email" value="<?php echo $aData["objCustomer"]->email; ?>">
                </div>
            </div>
            <?php if($action == "insertCustomer"){ ?>
            <div class="row pad-bottom">
                <div class="col-md-4 text-right-responsive">
                    <strong>
                       Created By
                    </strong>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" ' required name="CreatedBy" value="<?php echo $aData["objCustomer"]->createdBy; ?>">
                </div>
            <?php } else { ?>
                <div class="row pad-bottom">
                    <div class="col-md-4 text-right-responsive">
                        <strong>
                            Modified By
                        </strong>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" required name="ModifiedBy" value="<?php echo $aData["objCustomer"]->modifiedBy; ?>">
                    </div>
            <?php } ?>
  </div>

<!-- JAM: Processing off of the button value is generally a bad idea. -->
<?php if($action == "insertCustomer"){ ?>
<input type="submit" name="insert" class="btn btn-primary" value="Add" text="Insert" />
<?php } else { ?>
<input type="hidden" name="CustomerID" value='<?php echo $aData["objCustomer"]->id ?>'>
<input type="submit" name="update" value="Update" class="btn btn-primary" text="Update"/>
<?php } ?>
                <a>Cancel</a>
</form>
 </div>
