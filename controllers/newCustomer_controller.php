<?php
  class NewCustomerController {
  private $aData;

  // Note: Might be a better way to pass data around, however due to time constraints I'm just going to use a reference to avoid modifying more code structure than necessary.
  public function __construct(array &$aData)
  {
      $this->aData = &$aData;
  }

  public function home() {
    // $countries = Country::all();
    // $states = State::all();
    $this->aData["aStates"] = State::all();
    $this->aData["aCountries"] = Country::all();
    $req = "";
    $customer = new Customer();
    // require_once('views/newcustomer/home.php');
  }

  public function validateCustomer()
  {
      if(!isset($this->aData["objCustomer"])){
          // Avoiding overwriting the customer in the event of an edit.
          $this->aData["objCustomer"] = new Customer();
      }

      // JAM: Default values to empty.
      $this->aData["objCustomer"]->name = "";
      $this->aData["objCustomer"]->title = "";
      $this->aData["objCustomer"]->address = "";
      $this->aData["objCustomer"]->city = "";
      $this->aData["objCustomer"]->stateID = "";
      $this->aData["objCustomer"]->countryID = "";
      $this->aData["objCustomer"]->postalCode = "";
      $this->aData["objCustomer"]->email = "";
      $this->aData["aCountries"] = Country::all();
      $this->aData["aStates"] = State::all();

      // JAM: Use the same process for both an insert/update.
      if(isset($_POST["CustomerName"])) {

          // JAM: Sanitize any non-required fields.
          if (isset($_POST["CustomerTitle"])) {
              $this->aData["objCustomer"]->title = clean($_POST["CustomerTitle"]);
          }
          if (isset($_POST["Address"])) {
              $this->aData["objCustomer"]->address = clean($_POST["Address"]);
          }
          if (isset($_POST["City"])) {
              $this->aData["objCustomer"]->city = clean($_POST["City"]);
          }
          if (isset($_POST["PostalCode"])) {
              $this->aData["objCustomer"]->postalCode = clean($_POST["PostalCode"]);
          }

          // JAM: Required fields.
          if (strlen(trim($_POST["CustomerName"])) <= 0) {
              addMsg("error", "Please enter a customer name");
          } else {
              $this->aData["objCustomer"]->name = clean($_POST["CustomerName"]);
          }

          if (!isset($_POST["state"]) || (isset($_POST["state"]) && !isValidOption($_POST["state"], $this->aData["aStates"]))) {
              addMsg("error", "Please select a valid state");
          } else {
              $this->aData["objCustomer"]->stateID = clean($_POST["state"]);
          }

          if (!isset($_POST["country"]) || (isset($_POST["country"]) && !isValidOption($_POST["country"], $this->aData["aCountries"]))) {
              addMsg("error", "Please select a valid country");
          } else {
              $this->aData["objCustomer"]->countryID = clean($_POST["country"]);
          }

          if (!isset($_POST["Email"]) || (isset($_POST["Email"]) && !isValidEmail($_POST["Email"]))) {
              addMsg("error", "Please enter a valid email address");
          } else {
              $this->aData["objCustomer"]->email = clean($_POST["Email"]);
          }

          if (isset($_POST["insert"])) {
              if (!isset($_POST["CreatedBy"]) || strlen(trim($_POST["CreatedBy"])) <= 0) {
                  addMsg("error", "Please enter the name of the person creating this user");
              } else {
                  $this->aData["objCustomer"]->createdBy = clean($_POST["CreatedBy"]);
              }
          }

          if (isset($_POST["update"])) {
              if (!isset($_POST["ModifiedBy"]) || strlen(trim($_POST["ModifiedBy"])) <= 0) {
                  addMsg("error", "Please enter the name of the person modifying this user");
              } else {
                  $this->aData["objCustomer"]->modifiedBy = clean($_POST["ModifiedBy"]);
              }
          }

          if (getErrorCount() == 0) {
              if (strlen($this->aData["objCustomer"]->name) < 4 || strlen($this->aData["objCustomer"]->name) > 30) {
                  addMsg("error", "Your customer name must be between four and 30 characters in length");
              }

              if (strlen($this->aData["objCustomer"]->email) > 50) {
                  addMsg("error", "Your customer email can be no longer than 50 characters in length");
              }

              if (strlen($this->aData["objCustomer"]->title) > 0 && strlen($this->aData["objCustomer"]->title) > 10) {
                  addMsg("error", "Your title can be no longer than 10 characters in length");
              }

              if (strlen($this->aData["objCustomer"]->address) > 0 && strlen($this->aData["objCustomer"]->title) > 50) {
                  addMsg("error", "Your address can be no longer than 50 characters in length");
              }

              if (strlen($this->aData["objCustomer"]->address) > 0 && strlen($this->aData["objCustomer"]->title) > 15) {
                  addMsg("error", "Your city can be no longer than 15 characters in length");
              }

              if (strlen($this->aData["objCustomer"]->address) > 0 && !isValidZip($this->aData["objCustomer"]->postalCode)) {
                  addMsg("error", "Please enter a valid postal code");
              }

              if (isset($_POST["insert"])) {
                  if (strlen($this->aData["objCustomer"]->createdBy) < 4 || strlen($this->aData["objCustomer"]->createdBy) > 20) {
                      addMsg("error", "Your creating name must be between four and 20 characters in length");
                  }
              }

              if (isset($_POST["update"])) {
                  if (strlen($this->aData["objCustomer"]->modifiedBy) < 4 || strlen($this->aData["objCustomer"]->modifiedBy) > 20) {
                      addMsg("error", "Your modifying name must be between four and 20 characters in length");
                  }
              }
          }
      }
  }

  // @todo Redirect to notification pages on success.
  public function insertCustomer() {
      $this->validateCustomer();

      echo "<pre>Errors:\n" . print_r(aGetErrors(), true) . "</pre>\n";

      if(getErrorCount() == 0){
          if(isset($_POST["insert"])){
              $this->aData["objCustomer"]::insert($this->aData["objCustomer"]); // JAM: The customer arg could be optional, defaulting to the object's own properties?
          }
      }

      if(isset($_POST['insert'])){
          // require_once('views/newcustomer/home.php');
      }
      // JAM: Moved this section to a separate action, based on URL variables.
      /* else if(isset($_POST['delete'])){
        Customer::delete($_POST['CustomerID']);
        $this->aData["aCustomers"] = Customer::all();
        // require_once('views/newcustomer/customerList.php');
      } */
      // JAM: This section is also now in its own section.
      /*
      else if(isset($_POST['edit'])){
        $this->aData["aCountries"] = Country::all();
        $this->aData["aStates"] = State::all();
        $this->aData["objCustomer"] = Customer::getById($_POST['CustomerID']);
        // require_once('views/newcustomer/customerEditForm.php');

      } */
       
  }

  public function customerList(){
  	$this->aData["aCustomers"] = Customer::all();
    // require_once('views/newcustomer/customerList.php');
  }

  public function updateCustomer(){
      $this->aData["aCountries"] = Country::all();
      $this->aData["aStates"] = State::all();
      if(isset($_POST["CustomerID"])){
          $this->aData["objCustomer"] = Customer::getById($_POST['CustomerID']);
          // $this->aData["objCustomer"] = $this->aData["objCustomer"][0]; // JAM: Apparently, this can return multiple rows...
      }
    if(isset($_POST['update'])){
      $this->validateCustomer();

        echo "<pre>Errors:\n" . print_r(aGetErrors(), true) . "</pre>\n";

        if(getErrorCount() == 0) {
            $customer = Customer::update($this->aData["objCustomer"]);
            $this->aData["aCustomers"] = Customer::all();
            // require_once('views/newcustomer/customerList.php');
        }
    }
  }

  public function deleteCustomer()
  {
      // @todo Validate/sanitize.
      Customer::delete($_GET['CustomerID']);
      $this->aData["aCustomers"] = Customer::all();
  }

  public function error() {
    require_once('views/pages/error.php');
  }
}
?>