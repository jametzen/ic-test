<?php
  class PagesController {
    private $aData;

    // Note: Might be a better way to pass data around, however due to time constraings I'm just going to use a reference to avoid modifying more code structure than necessary.
    public function __construct(array &$aData)
    {
      $this->aData = &$aData;
    }

    public function home() {
      // $first_name = 'Gokul';
      // $last_name  = 'Nippani';

      $this->aData["strFirstName"] = "Foo";
      $this->aData["strLastName"] = "Bar";
      // require_once('views/pages/home.php');
    }

    public function error() {
      require_once('views/pages/error.php');
    }
  }
?>