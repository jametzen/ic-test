<?php
  class Db {
    private static $instance = NULL;

    private function __construct() {}

    private function __clone() {}

    public static function getInstance() {
      if (!isset(self::$instance)) {
        // JAM: Undeclared array?
        $pdo_options = array(); // @todo Take these from somewhere else so they can be provided via a config file, etc.

        // JAM: Exceptions are not typically the model that I use, though they can be useful. My rule of thumb is to only use exceptions in situations where you're okay with the app crashing
        // if something is unhandled. Most of the time, people aren't okay with the app crashing.
        $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        // JAM: Using root is a bad idea, and also fails on my system because root has no external access.
        self::$instance = new PDO('mysql:host=localhost;dbname=summerbreeze', 'mysqlweb', '*Interwebz*', $pdo_options); // @todo Take credentials from elsewhere, for security.
      }
      return self::$instance;
    }
  }
?>