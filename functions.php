<?php
/**
 * Created by PhpStorm.
 * User: jerem
 * Date: 9/25/2016
 * Time: 7:19 PM
 */

function isValidController($strController, $strAction)
{
    $aControllers = array('pages' => ['home', 'error'],'newCustomer'=>['home', 'error','insertCustomer','customerList','updateCustomer', 'deleteCustomer']);

    if (array_key_exists($strController, $aControllers)) {
        if (in_array($strAction, $aControllers[$strController])) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function clean($strVar)
{
    return trim(htmlentities($strVar, ENT_QUOTES));
}

// @todo Convert to a class.
function addMsg($strLevel, $strMsg)
{
    global $aMsgs;

    $aMsgs[$strLevel][] = $strMsg;
}

function isValidOption($optionVal, $aOptions)
{
    if(!ctype_digit($optionVal)){
        return false;
    }

    foreach($aOptions as $objOption){
        // @todo Modify to not rely on id.
        if($objOption->id == $optionVal){
            return true;
        }
    }

    return false;
}

function isValidEmail($strEmail)
{
    return preg_match("/^[a-zA-Z0-9]+([\\._\\-A-Za-z0-9])*@([A-Za-z0-9_\\-]+\\.)*([A-Za-z0-9]+)\\.([A-Za-z]{2,6})$/i", $strEmail);
}

// JAM: Unfortunately, this is probably US-compatible only.
function isValidZip($zipCode)
{
    return preg_match("/^([0-9]{5}(?:-[0-9]{4})?)*$/", $zipCode);
}

function getErrorCount()
{
    global $aMsgs;

    return count($aMsgs["error"]);
}

function aGetErrors()
{
    global $aMsgs;

    return $aMsgs["error"];
}
